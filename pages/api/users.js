import prisma from '../../lib/prisma'

const handle = async (req, res) => {
    const users = await prisma.$queryRaw`SELECT gps_seconds, raw_image, altitude_ellipsoidal_m, roll_deg, pitch_deg, heading_deg, projected_x_m, projected_y_m, projected_z_m, report_dt, region_code, amp_code, tam_code, dlc_code, raw_run, image_id, run_id, geom_id, prov_code, "year", "month", "day", st_x("geom") as x, st_y("geom") as y
    FROM public.mobile_mapping_systems`
    // const users = await prisma.$queryRaw`select *,st_x("geom") as x, st_y("geom") as y from mobile_mapping_systems`
    // const users = await prisma.user.findMany()
    console.log('file: users.js ~ line 5 ~ users', users)
    res.json(users)
}

export default handle